
FROM nginx:1.17.10-alpine
COPY ./conf/default.conf /etc/nginx/conf.d/


CMD ["nginx", "-g", "daemon off;"]
