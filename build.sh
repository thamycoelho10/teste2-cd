#!/bin/bash
set -e
set -x

#prepara as imagens docker de back e front e as disponibiliza no dockerhub, para uso nos servidores web

sudo docker build -t thamiris2020/api back/.
sudo docker push thamiris2020/api

sudo docker build -t thamiris2020/web front/.
sudo docker push thamiris2020/
