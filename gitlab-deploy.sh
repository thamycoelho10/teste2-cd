#!/bin/bash

#Get servers list
set -f
server_web=$IP_ADDRESS
server_web=($string1)

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
  echo "Deploying information to EC2 and Gitlab"
  echo "Deploy project on server ${array[i]}"
  ssh ubuntu@${array[i]} "cd  /srv/dashboard && docker-compose down --remove-orphans && docker-compose up --build -d"
done